/********************************************************************\

  Name:         k600_epics_beamline.h
  Created by:   Lee Pool

  Contents:     Channel Access device driver function declarations


\********************************************************************/

#define DT_DEVICE      1
#define DT_BEAMBLOCKER 2
#define DT_PSA         3
#define DT_SEPARATOR   4

INT k600_epics(INT cmd, ...);
